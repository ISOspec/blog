<?php

class m160530_111317_tbl_comment extends CDbMigration
{
	public function up()
	{
         $this->createTable('tbl_comment', array(
            'id' => 'pk',
            'title' => 'string NOT NULL',
            'content' => 'text',
        ));
	}

	public function down()
	{
		echo "m160530_111317_tbl_comment does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}